import React from 'react';
import { Navbar } from 'react-bootstrap';

const Header = () => (
    <Navbar bg="light" expand="lg">
        <Navbar.Brand href="/">Chris+tmas Movie Library</Navbar.Brand>
    </Navbar>
);

export default Header;
