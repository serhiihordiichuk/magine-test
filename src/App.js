import React, { Component, lazy, Suspense } from 'react';
import { withRouter } from 'react-router';
import { Redirect, Route, Switch } from 'react-router-dom';

import { Container } from 'react-bootstrap';

import Header from './components/Header'

const Home = withRouter(
    lazy(() => import(/* webpackChunkName: "home" */ './pages/home/Index'))
);
const MovieDetails = withRouter(
    lazy(() => import(/* webpackChunkName: "movie-details" */ './pages/movie-details/Index'))
);

class App extends Component {
    render() {
        return (
            <div className="App">
                <Header/>
                <Container>
                    <Suspense fallback={<div>Loading...</div>}>
                        <Switch>
                            <Redirect from="/" exact to="/home"/>
                            <Route path="/home" component={Home}/>
                            <Route path="/movie-details/:id" component={MovieDetails}/>
                        </Switch>
                    </Suspense>
                </Container>
            </div>
        );
    }
}

export default App;
