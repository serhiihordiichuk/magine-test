import React, { useEffect, useState } from 'react';
import { Row, Col, Image, Button } from 'react-bootstrap';
import styled from 'styled-components'

import useMovies from '../../services/useMovies';

const Wrapper = styled.section`
    padding: 4em 0;
`;
const Card = styled.div`
    position: relative;
    margin: 0 0 1em;
    &:hover div{
        display: flex;
    }
`;
const CardTitle = styled.div`
    display: none;
    align-items: center;
    justify-content: center;
    text-align: center;
    position: absolute;
    font-size: 16px;
    font-weight: bold;
    padding: 1em;
    top: 0;
    left: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,0.4);
    color: #ffffff;
    cursor: pointer;
`;

function Home({ history }) {
    const [page, setPage] = useState(1);
    const {
        movies,
        getMovies,
        selectMovie
    } = useMovies();

    useEffect(() => {
        if (movies.length / 10 < page) {
            getMovies({ query: 'christmas', page: page });
        }
    }, [movies, getMovies, page]);

    function handleSelectMovie(selectedMovie) {
        selectMovie(selectedMovie);
        history.push(`/movie-details/${selectedMovie.imdbID}`);
        console.log(`you selected ${selectedMovie.Title}`);
    }

    return (
        <Wrapper>
            <Row>
                {movies.map((movie, index) => (
                    <Col key={movie.imdbID} xs={6} lg={4} xl={2}>
                        <Card onClick={() => handleSelectMovie(movie)}>
                            <Image src={movie.Poster} alt={movie.imdbID} thumbnail/>
                            <CardTitle>{movie.Title}</CardTitle>
                        </Card>
                    </Col>
                ))}
                {page > 0 && <Col xs={6} md={2}>
                    <Button variant="primary" onClick={() => setPage(page + 1)}>Load More</Button>
                </Col>}
            </Row>
        </Wrapper>
    );
}

export default Home;
