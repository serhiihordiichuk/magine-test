import React, { useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import styled from 'styled-components'

import useMovies from '../../services/useMovies';

const Wrapper = styled.section`
    padding: 4em 0;
`;

function MovieDetails({ history }) {
    const {
        selectedMovie,
    } = useMovies();

    useEffect(() => {
        console.log(selectedMovie);
        if (!selectedMovie) {
            history.push('/');
        }
    }, [selectedMovie, history]);

    return (
        <Wrapper>
            <Card style={{ width: '100%' }}>
                <Card.Img variant="top" src={selectedMovie?.Poster}/>
                <Card.Body>
                    <Card.Title>{selectedMovie?.Title}</Card.Title>
                    <Card.Text>
                        <b>Year: </b>{selectedMovie?.Year}
                    </Card.Text>
                    <Button variant="primary" onClick={() => history.push('/')}>Go Back</Button>
                </Card.Body>
            </Card>
        </Wrapper>
    );
}

export default MovieDetails;
