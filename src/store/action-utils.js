export const parseList = response => {
    console.log(response);
    if (response.status !== 200) throw Error(response.message);
    let list = response.data.Search;
    if (typeof list !== 'object') {
        list = [];
    }
    return list;
};
