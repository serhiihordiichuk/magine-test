import axios from 'axios';
import { parseList } from './action-utils';
import API from './config';

export const loadMoviesApi = async (options) => {
    const response = await axios.get(`${API}?type=movie&s=${options.query}&page=${options.page || 1}&apikey=c88a66b1`);
    return parseList(response, 200);
};
