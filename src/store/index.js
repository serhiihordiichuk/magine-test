import { combineReducers } from 'redux';
import { moviesReducer, selectedMovieReducer } from './movie.reducer';

export * from './movie.reducer';
export * from './movie.actions';
export * from './movie.saga';

const store = combineReducers({
    movies: moviesReducer,
    selectedMovie: selectedMovieReducer,
});

export default store;
