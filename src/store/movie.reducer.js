import {
    LOAD_MOVIE,
    LOAD_MOVIE_SUCCESS,
    LOAD_MOVIE_ERROR,
    SELECT_MOVIE
} from './movie.actions';

let initState = {
    loading: false,
    data: [],
    error: void 0
};

export const moviesReducer = (state = initState, action) => {
    switch (action.type) {
        case LOAD_MOVIE:
            return { ...state, loading: true, error: '' };
        case LOAD_MOVIE_SUCCESS:
            return { ...state, loading: false, data: [...state.data, ...action.payload] };
        case LOAD_MOVIE_ERROR:
            return { ...state, loading: false, error: action.payload };
        default:
            return state;
    }
};

let initialSelectedMovie = null;

export const selectedMovieReducer = (state = initialSelectedMovie, action) => {
    switch (action.type) {
        case SELECT_MOVIE:
            return action.payload ? { ...action.payload } : null;
        default:
            return state;
    }
};
