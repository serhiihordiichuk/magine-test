export const LOAD_MOVIE = '[Movies] LOAD_MOVIE';
export const LOAD_MOVIE_SUCCESS = '[Movies] LOAD_MOVIE_SUCCESS';
export const LOAD_MOVIE_ERROR = '[Movies] LOAD_MOVIE_ERROR';

export const SELECT_MOVIE = '[Movies] SELECT_MOVIE';

export const loadMoviesAction = options => ({ type: LOAD_MOVIE, payload: options });
export const selectMovieAction = movie => ({ type: SELECT_MOVIE, payload: movie });
