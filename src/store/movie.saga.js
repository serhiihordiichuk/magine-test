import { put, takeEvery, call, all } from 'redux-saga/effects';

import { LOAD_MOVIE, LOAD_MOVIE_SUCCESS, LOAD_MOVIE_ERROR } from './movie.actions';
import { loadMoviesApi } from './movie.api';

export function* loadingMoviesAsync({ payload }) {
    try {
        const data = yield call(loadMoviesApi, payload);
        const movies = [...data];

        yield put({ type: LOAD_MOVIE_SUCCESS, payload: movies });
    } catch (err) {
        yield put({ type: LOAD_MOVIE_ERROR, payload: err.message });
    }
}

export function* watchLoadingMoviesAsync() {
    yield takeEvery(LOAD_MOVIE, loadingMoviesAsync);
}

export function* movieSaga() {
    yield all([
        watchLoadingMoviesAsync(),
    ]);
}
