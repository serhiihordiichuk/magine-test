import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
    loadMoviesAction, selectMovieAction
} from '../store';

function useMovies() {
    const dispatch = useDispatch();

    return {
        movies: useSelector(state => state.movies.data),
        selectedMovie: useSelector(state => state.selectedMovie),

        getMovies: useCallback((options) => dispatch(loadMoviesAction(options)), [dispatch]), // called within a useEffect()
        selectMovie: movie => dispatch(selectMovieAction(movie)),
    };
}

export default useMovies;
